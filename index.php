<?php 
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1); 
require 'vendor/autoload.php'; 
Twig_Autoloader::register();  
$app = new \Slim\Slim();  
$loader = new Twig_Loader_Filesystem('templates');  
$twig = new Twig_Environment($loader, array(  /*'cache' => 'cache',*/ ));  
//BASE URL
session_start();

define('BASE_URL', 'http://localhost:8080/new-project-admin/');  //Sobreescribir por la ruta de su proyecto.
$data=array(
	'BASE_URL' => constant('BASE_URL'),
);

$app->get('/', function() use ($twig,$app,$data){  
    if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"])){
		$app->response->redirect($app->urlFor('login'), 303);
	}
	else{
		$data['session']= $_SESSION["user_session"];
		echo $twig->render('home.html',$data);
	}    
})->name('admin');


$app->map('/login/', function () use ($twig,$app,$data) {
    $message=0;
    if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"])){
		if($app->request()->isPost()) {
	    	include 'clases/user.php';
	    	$message = User::Login($_POST["username"], $_POST["password"]);
	    	if ($message == "success") {
	    		$app->response->redirect($app->urlFor('admin'), 303);
	    	}
	    	else{
	    		$data['error'] = $message;
	    	}	
    	}
		echo $twig->render('login.html',$data);
	}
	else{
		$app->response->redirect($app->urlFor('admin'), 303);
	}	
})->via('GET', 'POST')->name('login');


$app->get('/logout/', function() use ($twig,$app,$data){  
    include 'clases/user.php';
	User::Logout();
	$app->response->redirect($app->urlFor('login'), 303);
})->name('logout');


$app->map('/register/', function() use ($twig,$app,$data){  
	if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"]) || !$_SESSION["user_session"]["is_admin"]){
    	$app->response->redirect($app->urlFor('login'), 303);
	}
	else{
	  	if($app->request()->isPost()) {
			include 'clases/user.php';
	    	$error = User::Register($_POST["usuario"], $_POST["password"], $_POST["password1"]);
	    	$data['error']=$error;
		}
		$data['session']= $_SESSION["user_session"];
		echo $twig->render('register.html',$data);
  	}
})->via('GET', 'POST')->name('register');


$app->run();  
?>