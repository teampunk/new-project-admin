<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

include 'conexion.php';
define("KEY", "aghsythsksn44df:");

class User{
     
    public static function Login($myusername,$mypassword){
        if (empty($myusername) || empty($mypassword)){
           $message = "Complete all fields";
        }
        if(!isset($message)){
            $con = new Conexion();
            $sth = $con->conexion->prepare('
                SELECT
                *
                FROM users
                WHERE
                username = :username
                LIMIT 1
            ');
            $sth->bindParam(':username', $myusername);
            $sth->execute();
            $count = $sth->rowCount();
            if ($count > 0){
                $user = $sth->fetch(PDO::FETCH_OBJ);
                if ( $user->password == crypt($mypassword, $user->password) ) {
                    $_SESSION["user_session"]["id"]=$user->id;
                    $_SESSION["user_session"]["username"]=$user->username;
                    $_SESSION["user_session"]["is_admin"]=$user->is_admin;
                    $message =  "success";
                }
                else{
                    $message = 'Incorrect username or password'; 
                }
            }
            else{
                $message = 'Incorrect username or password'; 
            }
        }
        else{
                $message = "Error occured: ".$message;
            }
        return $message;   
        
   
    }

    public static function Logout(){
        unset ($_SESSION["user_session"]);
        
    }

    public static function Register($myusername,$password,$password1){
        
        if (empty($myusername) || empty($password) || empty($password1)){
            $message = "Complete all fields";
        }
        if ($password != $password1){
            $message = "Passwords don't match";
        }
        if (strlen($password) <= 5){
            $message = "Choose a password longer then 5 character";
        }
        if(!isset($message)){
            try {
                $con = new Conexion();
                $con->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $cost = 10;
                $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
                $salt = sprintf("$2a$%02d$", $cost) . $salt;
                $hash = crypt($password, $salt);
                $query = 'SELECT username FROM users WHERE username = :myusername';
                $sth = $con->conexion->prepare($query);
                $sth->bindParam(':myusername',$myusername);
                
                $isQueryOk = $sth->execute();
                
                if ($isQueryOk){
                    $count = $sth->rowCount();
                    if ($count > 0){
                         $message = "Error: User <strong>".$myusername." </strong>already exists";
                    }
                    else{
                        $query = 'INSERT INTO users (username,password) VALUES (:myusername,:hash)';
                        $sth = $con->conexion->prepare($query);
                        $sth->bindParam(':myusername',$myusername);
                        $sth->bindParam(':hash',$hash);
                        $sth->execute();
                        $message = "success";
                        $_SESSION["user_session"]["user_created"] = $myusername;
                    }
                }
                else{
                    trigger_error('Error executing statement.', E_USER_ERROR);
                }       
                
             } catch (PDOException $e) {
                print "¡Error!: " . $e->getMessage() . "";
            } 
        }else{
            $message = "Error: ".$message;
            
        }
        return $message;
           
    }         
}
?>
